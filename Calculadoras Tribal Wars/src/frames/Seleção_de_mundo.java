package frames;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import main.Main;
import database.MundoSelecionado;

@SuppressWarnings("serial")
public class Sele��o_de_mundo extends JFrame {
	
	private JTextField velocidade;
	private JTextField modificador;
	
	JCheckBox chckbxArqueiros;
	JCheckBox chckbxFerreiroDeNveis;
	JCheckBox chckbxAcademiaPorAmazenamento;
	JCheckBox chckbxPaladino;
	JCheckBox chckbxIgreja;
	JCheckBox chckbxMilcia;
	JCheckBox chckbxMoral;
	
	public Sele��o_de_mundo() {
		
		getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblVelocidadeDoMundo = new JLabel("Velocidade do Mundo");
		getContentPane().add(lblVelocidadeDoMundo);
		
		velocidade = new JTextField();
		getContentPane().add(velocidade);
		velocidade.setColumns(10);
		
		velocidade.setDocument(new PlainDocument() {
			
			public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
				    if (str == null)
				      return;

				    if ((getLength() + str.length()) <= 4) {
				    	if (Character.isDigit(str.charAt(0)))
				    		super.insertString(offset, str, attr);
				    	else if ((str.charAt(0) == '.' || str.charAt(0) == ',') && !getText(0, getLength()).contains(".")){
				    			if (getText(0, getLength()).equals(""))
				    				 super.insertString(offset, "0,", attr);
				    			else 
				    				super.insertString(offset, ".", attr);
				    	}
				    }
				  }
		});
		velocidade.setText("1");
		
		JLabel lblModificadorDeUnidade = new JLabel("Modificador de Unidade");
		getContentPane().add(lblModificadorDeUnidade);
		
		modificador = new JTextField();
		getContentPane().add(modificador);
		modificador.setColumns(10);
		
		modificador.setDocument(new PlainDocument() {
			
			@Override
			 public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
				if (str == null)
				      return;

				    if ((getLength() + str.length()) <= 4) {
				    	if (Character.isDigit(str.charAt(0)))
				    		super.insertString(offset, str, attr);
				    	else if ((str.charAt(0) == '.' || str.charAt(0) == ',') && !getText(0, getLength()).contains("."))
				    		 super.insertString(offset, ".", attr);
				    
				    }
				  }
		});
		modificador.setText("1");
		
		chckbxArqueiros = new JCheckBox("Arqueiros");
		getContentPane().add(chckbxArqueiros);
		
		chckbxFerreiroDeNveis = new JCheckBox("Ferreiro de N\u00EDveis");
		getContentPane().add(chckbxFerreiroDeNveis);
		
		chckbxAcademiaPorAmazenamento = new JCheckBox("Academia por Amazenamento");
		getContentPane().add(chckbxAcademiaPorAmazenamento);
		
		chckbxPaladino = new JCheckBox("Paladino");
		getContentPane().add(chckbxPaladino);
		
		chckbxIgreja = new JCheckBox("Igreja");
		getContentPane().add(chckbxIgreja);
		
		chckbxMilcia = new JCheckBox("Mil\u00EDcia");
		getContentPane().add(chckbxMilcia);
		
		chckbxMoral = new JCheckBox("Moral");
		getContentPane().add(chckbxMoral);
		
		JButton btnIniciar = new JButton("Iniciar");
		getContentPane().add(btnIniciar);
		
		btnIniciar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
			
			BigDecimal speed;		
			try {	
				speed = new BigDecimal(velocidade.getText());	
			} catch (NumberFormatException e) {
				speed = BigDecimal.ONE;
			}
			
			BigDecimal modifier;		
			try {	
				modifier = new BigDecimal(modificador.getText());	
			} catch (NumberFormatException e) {
				modifier = BigDecimal.ONE;
			}
			
			MundoSelecionado.setDados(chckbxArqueiros.isSelected(),chckbxMilcia.isSelected(), 
					chckbxPaladino.isSelected(), chckbxIgreja.isSelected(), 
					chckbxAcademiaPorAmazenamento.isSelected(), 
					chckbxFerreiroDeNveis.isSelected(),chckbxMoral.isSelected(), speed, modifier);
			
			Main.openMainFrame();
			
			dispose();
			
			}
		});
		
	}

}
