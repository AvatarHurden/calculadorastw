package frames;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import database.Cores;
import database.Ferramenta;

@SuppressWarnings("serial")
public class MainWindow extends JFrame {
	
	JPanel tabs;
	JPanel body;
	
	public List<Ferramenta> ferramentas = new ArrayList<Ferramenta>();
	
	public MainWindow() {
		
		getContentPane().setBackground(Cores.FUNDO_CLARO);
		setBackground(Cores.FUNDO_CLARO);
		
		setLayout(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(5, 5, 0, 5);
		
		c.gridy = 0;
		c.gridx = 0;
		tabs = new JPanel();
		tabs.setLayout(new GridBagLayout());
		c.anchor = GridBagConstraints.WEST;
		add(tabs,c);
		
		c.insets = new Insets(0, 5, 5, 5);
		c.gridy++;
		c.fill = GridBagConstraints.HORIZONTAL;
		body = new JPanel();
		body.setLayout(new FlowLayout());
		body.setBackground(Cores.FUNDO_CLARO);
		body.setBorder(new LineBorder(Cores.SEPARAR_ESCURO));
		add(body,c);
		
	}
	
	public void addPanel(Ferramenta tool) {
		
		tool.setFrame(this);
		
		ferramentas.add(tool);
	
		tabs.add(tool.getTab());
		
		body.add(tool);
		
	}
	
	public void selectFirst() {
		
		for (Ferramenta i : ferramentas)
			i.setSelected(false);
		
		ferramentas.get(0).setSelected(true);
		
	}
	
}
