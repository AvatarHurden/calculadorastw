package config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class load {

	static List<Properties> propertyList = new ArrayList<Properties>();
	
	public static List<Mundo> mundoList = new ArrayList<Mundo>();
	
	public void test() throws IOException {
	
		try {
			
			BufferedReader in = new BufferedReader(new FileReader(
					new File("Configurações//configurações de mundo.txt")));
		
			String total = "";
			
			while ((total = in.readLine()) != null) {
			
				String s;
				total +="\n";
			
				while ((s = in.readLine()) != null) {
					if (!s.equals("#"))
						total += s+"\n";
					else
						break;
				}
			
				Properties i = new Properties();
				i.load(new StringReader(total));
				propertyList.add(i);
				
				Mundo mundo = new Mundo();
				mundo.setAll(i);
				mundoList.add(mundo);
			}
			
			in.close();
			
		} catch (Exception e) {
			
			propertyList.removeAll(propertyList);
			mundoList.removeAll(mundoList);
			
			System.out.println("O arquivo salvo está corrompido");
			
			BufferedReader in = new BufferedReader(
						new InputStreamReader(getClass().getResourceAsStream("default")));
			
			String total = "";
				
			while ((total = in.readLine()) != null) {
			
				String s;
				total +="\n";
			
				while ((s = in.readLine()) != null) {
					if (!s.equals("#"))
						total += s+"\n";
					else
						break;
				}
			
				Properties i = new Properties();
				i.load(new StringReader(total));
				propertyList.add(i);
				
				Mundo mundo = new Mundo();
				mundo.setAll(i);
				mundoList.add(mundo);
			}
				
			in.close();
			 
		}
	}

	public void save() {
		
		try {
			
			File configurações = new File("Configurações/configurações de mundo.txt");
			
			if (!configurações.exists())
				if (!new File("Configurações").exists())
					new File("Configurações").mkdir();
				configurações.createNewFile();
			
			FileOutputStream out = new FileOutputStream(configurações);
			
			for (Properties i : propertyList) {
				
				i.store(out, "");
				
			}
			
			out.close();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
