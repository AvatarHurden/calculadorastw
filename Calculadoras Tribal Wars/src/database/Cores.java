package database;

import java.awt.Color;

public class Cores {

	/**
	 * Cor para plano de fundo, vers�o escura
	 */
	public static final Color FUNDO_ESCURO = new Color(196, 166, 106);
	
	/**
	 * Cor para plano de fundo nas ferramentas
	 */
	public static final Color FUNDO_CLARO = new Color(251, 235, 201);
	
	/**
	 * Cor para separar linhas, vers�o escura
	 */
	public static final Color SEPARAR_ESCURO = new Color(146, 116, 56);
	
	/**
	 * Cor para separar linhas, vers�o clara
	 */
	public static final Color SEPARAR_CLARO = new Color(196, 166, 106);
	
	/**
	 * Cor para alternar entre linhas, vers�o escura
	 */
	public static final Color ALTERNAR_ESCURO = new Color(239, 223, 187);
	
	/**
	 * Cor para alternar entre linhas, vers�o clara
	 */
	public static final Color ALTERNAR_CLARO = new Color(245, 237, 216);
	
}
