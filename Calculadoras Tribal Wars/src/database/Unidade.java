package database;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

public enum Unidade{


	//                Nome				mad	  arg    fer   pop   atk   dg    dc    da    mov   saq  tempo
	
	LANCEIRO	  ("Lanceiro", 			50,    30,	  10,	 1,	  10,   15,   45,   20,	 18,   25,  680),
	ESPADACHIM	  ("Espadachim",		30,    30,	  70,	 1,	  25,   50,   15,   40,	 22,   15,  1000),
	ARQUEIRO	  ("Arqueiro",			100,   30,	  60,	 1,	  15,   50,   40,   5, 	 18,   10,  1200),
	B�RBARO		  ("B�rbaro", 			60,    30,	  40,	 1,	  40,   10,   5,    10,	 18,   10,  880),
	EXPLORADOR	  ("Explorador",		50,    50,	  20,	 2,	  0,	2,    1,    2,    9,    0,   600),
	CAVALOLEVE	  ("Cavalaria Leve",	125,   100,	  250,	 4,	  130,  30,   40,   30,   10,   80,  1200),
	ARCOCAVALO	  ("Arqueiro a Cavalo", 250,   100,	  150,	 5,	  120,  40,   30,   50,   10,   50,  1800),
	CAVALOPESADO  ("Cavalaria Pesada",  200,   150,	  600,	 6,	  150,  200,  80,   180,  11,   50,  2400),
	AR�ETE		  ("Ar�ete", 			300,   200,	  200,	 5,	  2,	20,   50,   20,   30,   0,   3200),
	CATAPULTA	  ("Catapulta", 		320,   400,	  100,	 8,	  100,  100,  50,   100,  30,   0,   4800),
	PALADINO	  ("Paladino", 			20,    20,	  40,	 10,  150,  250,  400,  150,  10,   100, 21600),
	NOBRE         ("Nobre", 		    40000, 50000, 50000, 100, 30,   100,  50,   100,  35,   0,   12000),
	MIL�CIA		  ("Mil�cia", 			0,     0,	  0,	 0,	  5,	15,   45,   25,   0.02, 0,   0);
	
	private final static BigDecimal lvl2 = new BigDecimal("1.25");
	private final static BigDecimal lvl3 = new BigDecimal("1.4");
	
	private final String nome;
	private final BigInteger madeira;
	private final BigInteger argila;
	private final BigInteger ferro;
	private final BigInteger popula��o;
	private final BigInteger ataque;
	private final BigInteger defGeral;
	private final BigInteger defCav;
	private final BigInteger defArq;
	private final BigDecimal velocidade;
	private final BigInteger saque;
	private final BigDecimal tempoProdu��o;

	/**
	 * @param nome da unidade
	 * @param custo de madeira
	 * @param custo de argila
	 * @param custo de ferro
	 * @param popula��o utilizada
	 * @param for�a de ataque
	 * @param defesa geral
	 * @param defesa cavalaria
	 * @param defesa arqueiros
	 * @param velocidade base
	 * @param saque
	 * @param tempo de produ��o em segundos (100%)
	 */
	private Unidade(String nome, int madeira, int argila, int ferro, int popula��o,
			int ataque, int defGeral, int defCav, int defArq, double velocidade,
			int saque, int tempo) {
		this.nome = nome;
		this.madeira = new BigInteger(String.valueOf(madeira));
		this.argila = new BigInteger(String.valueOf(argila));
		this.ferro = new BigInteger(String.valueOf(ferro));
		this.popula��o = new BigInteger(String.valueOf(popula��o));
		this.ataque = new BigInteger(String.valueOf(ataque));
		this.defGeral = new BigInteger(String.valueOf(defGeral));
		this.defCav = new BigInteger(String.valueOf(defCav));
		this.defArq = new BigInteger(String.valueOf(defArq));
		this.velocidade = new BigDecimal(String.valueOf(velocidade));
		this.saque = new BigInteger(String.valueOf(saque));
		tempoProdu��o = new BigDecimal(String.valueOf(tempo));
	}

	/**
	 * @return custo de madeira da unidade
	 */
	public BigInteger madeira() { return madeira; }

	/**
	 * @return custo de argila da unidade
	 */
	public BigInteger argila() { return argila; }

	/**
	 * @return custo de ferro da unidade
	 */
	public BigInteger ferro() { return ferro; }

	/**
	 * @return custo de popula��o da unidade
	 */
	public BigInteger popula��o() { return popula��o; }	

	/**
	 * @return for�a de ataque da unidade
	 */
	public BigInteger ataque() { return ataque; }

	/**
	 * @return defesa geral da unidade
	 */
	public BigInteger defGeral() { return defGeral; }

	/**
	 * @return defesa de cavalaria da unidade
	 */
	public BigInteger defCav() { return defCav; }

	/**
	 * @return defesa de arqueiro da unidade
	 */
	public BigInteger defArq() { return defArq; }

	/**
	 * @return velocidade base da unidade
	 */
	public BigDecimal velocidade() { return velocidade; }

	/**
	 * @return capacidade de saque da unidade
	 */
	public BigInteger saque() { return saque; }
	
	/**
	 * @return tempo de produ��o da unidade em segundos (100%)
	 */
	public BigDecimal tempoDeProdu��o() { return tempoProdu��o; }
	
	/**
	 * @return nome da unidade, com primeira letra da cada palavra mai�scula
	 */
	public String nome() { return nome;}
	
	/**
	 * Aumenta a quantidade dada pela raz�o do n�vel fornecido. 
	 * Se o n�vel for diferente de 2 ou 3, retorna o valor dado
	 */
	public BigInteger upgrade(int lvl, BigInteger value) {
		
		BigDecimal total = new BigDecimal(value.toString());
		
		if (lvl == 2)
			total = total.multiply(lvl2);
		else if (lvl == 3)
			total = total.multiply(lvl3);
		else
			return value;
		
		return total.setScale(0,RoundingMode.HALF_UP).toBigInteger();

	}
	
}
