package main;

import java.io.IOException;

import javax.swing.JFrame;

import frames.MainWindow;
import frames.Selecionar;
import frames.Sele��o_de_mundo;

public class Main {

	static Sele��o_de_mundo selecionar;
	static MainWindow mainFrame;
	
	public static void main(String[] args) {
		
//		for (Edif�cio i : Edif�cio.values())
//			System.out.println(i.nome()+": "+i.pontosTe�ricaMatch());
//		
		
		config.load test = new config.load();
		
		try {
			test.test();
		} catch (IOException e) {
//			 TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Selecionar select = new Selecionar();
		
		select.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		select.setVisible(true);
		select.pack();
		
//		openSelection();
		
	}
	
	public static void openSelection() {
		
		selecionar = new Sele��o_de_mundo();
		
		selecionar.setSize(289,220);
		selecionar.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		selecionar.setVisible(true);
		
	}
	
	public static void openMainFrame() {
		
		mainFrame = new MainWindow();
		mainFrame.addPanel(new recrutamento.GUI());
		mainFrame.addPanel(new dados_de_unidade.GUI());
		mainFrame.addPanel(new pontos.GUI());
		mainFrame.addPanel(new dist�ncia.GUI());
		mainFrame.addPanel(new oponentes_derrotados.GUI());
		
		mainFrame.selectFirst();
		
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		mainFrame.setUndecorated(true);
		mainFrame.setVisible(true);
		mainFrame.pack();
		mainFrame.setResizable(false);
		
	}
	
}
