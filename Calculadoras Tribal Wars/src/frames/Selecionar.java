package frames;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import main.Main;
import config.Mundo;
import config.load;
import database.Cores;
import database.MundoSelecionado;

@SuppressWarnings("serial")
public class Selecionar extends JFrame {
	
	JComboBox<String> comboBox;
	
	public Selecionar() {
		
		setResizable(false);
		
		getContentPane().setBackground(Cores.ALTERNAR_ESCURO);
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		JLabel lblTítulo = new JLabel("");
		GridBagConstraints gbc_lblTítulo = new GridBagConstraints();
		gbc_lblTítulo.anchor = GridBagConstraints.CENTER;
		gbc_lblTítulo.insets = new Insets(0, 0, 5, 5);
		gbc_lblTítulo.gridx = 0;
		gbc_lblTítulo.gridwidth = 1;
		gbc_lblTítulo.gridy = 0;
		getContentPane().add(lblTítulo, gbc_lblTítulo);
		
		lblTítulo.setIcon(new ImageIcon("Título.png"));
		
		JPanel panelMundo = new JPanel();
		
		panelMundo.setOpaque(false);
		
		GridBagConstraints gbc_panelMundo = new GridBagConstraints();
		gbc_panelMundo.anchor = GridBagConstraints.CENTER;
		gbc_panelMundo.insets = new Insets(0, 0, 0, 5);
		gbc_panelMundo.gridx = 0;
		gbc_panelMundo.gridy = 1;
		getContentPane().add(panelMundo, gbc_panelMundo);
		
		JLabel lblMundo = new JLabel("");
		panelMundo.add(lblMundo);
		
		List<String> nomeMundos = new ArrayList<String>();
		
		for (Mundo i : load.mundoList)
			nomeMundos.add(i.getNome());
		
		comboBox = new JComboBox<String>(nomeMundos.toArray(new String[nomeMundos.size()]));
		
		panelMundo.add(comboBox);
		
		JButton btnIniciar = new JButton("Iniciar");
		
		btnIniciar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
			
				MundoSelecionado.setMundo(load.mundoList.get(comboBox.getSelectedIndex()));
				
				Main.openMainFrame();
				
				dispose();
				
			}
		});
		
		GridBagConstraints gbc_btnIniciar = new GridBagConstraints();
		gbc_btnIniciar.anchor = GridBagConstraints.CENTER;
		gbc_btnIniciar.insets = new Insets(0, 0, 10, 5);
		gbc_btnIniciar.gridx = 0;
		gbc_btnIniciar.gridy = 2;
		getContentPane().add(btnIniciar, gbc_btnIniciar);
		
	}

}
